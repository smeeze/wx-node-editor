// wx
#include <wx/wx.h>
// smeeze wx
#include <smeeze/wx/node_editor/control.h>
// self
#include <apps/node_editor/model.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
class my_app : public wxApp {
public:
  void load(const std::string &filename) { _md.load(filename); }

protected:
  bool OnInit() override {
    auto *d = new smeeze::wx::node_editor::control(&_md, nullptr, wxID_ANY,
                                                   "smeeze wx node editor");
    d->Show();
    return true;
  }

private:
  apps::node_editor::model _md;
};
} // namespace
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
  my_app *a(new my_app());
  if (argc > 1) {
    a->load(argv[1]);
  }
  wxApp::SetInstance(a);
  wxEntryStart(argc, argv);
  wxTheApp->CallOnInit();
  wxTheApp->OnRun();
  wxTheApp->OnExit();
  wxEntryCleanup();
  return 0;
}
