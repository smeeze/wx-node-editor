#pragma once

// smeeze wx
#include <smeeze/wx/node_editor/model.h>

namespace apps {
namespace node_editor {
class model : public smeeze::wx::node_editor::model {
protected:
  //
  std::vector<std::string> _node_list_available() const override;
  std::string _node_add(const std::string &s) override;
  void _node_del(const std::string &id) override;
  void _node_rename(const std::string &, const std::string &) override;
  void _node_inc_position(const std::string &, const smeeze::geom::vec2<float> &) override;
  void _node_set_position(const std::string &, const smeeze::geom::vec2<float> &) override;
  const std::vector<smeeze::wx::node_editor::node> &_get_nodes() const override;
  void _node_to_back(const std::string &) override;
  std::string _node_info(const std::string &) override;
  //
  bool _connection_is_valid(
      const smeeze::wx::node_editor::connection &c) const override;
  void _connection_add(const smeeze::wx::node_editor::connection &c) override;
  void _connection_del(const smeeze::wx::node_editor::connection &c) override;
  const std::vector<smeeze::wx::node_editor::connection> &
  _get_connections() const override;

  void _set_option(const std::string &, const std::string &,
                   const std::string &) override;

  wxWindow *_get_output_window(wxWindow *, const std::string &,
                               const std::string &) override;
  std::string _output_info(const std::string &, const std::string &) override;

  void _clear() override;

protected:
  std::string _get_free_id() const;

protected:
  std::vector<smeeze::wx::node_editor::node> _nodes;
  std::vector<smeeze::wx::node_editor::connection> _connections;

};
} // namespace node_editor
} // namespace apps
