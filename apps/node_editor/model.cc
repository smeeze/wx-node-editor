// system
#include <experimental/vector>
#include <fstream>
// fmt
#include <fmt/format.h>
// wx
#include <wx/stattext.h>
// self
#include <apps/node_editor/model.h>
///////////////////////////////////////////////////////////////////////////////
std::vector<std::string>
apps::node_editor::model::_node_list_available() const {
  return {{"one", "two", "three"}};
}
///////////////////////////////////////////////////////////////////////////////
std::string apps::node_editor::model::_node_add(const std::string &s) {
  // options
  std::vector<smeeze::wx::node_editor::node::option> options;
  if (s == "one") {
    options.emplace_back<smeeze::wx::node_editor::node::option>(
        {"str", "none"});
    options.emplace_back<smeeze::wx::node_editor::node::option>(
        {"file_in",
         "test.txt",
         smeeze::wx::node_editor::node::option_type::file_load,
         {{"desc", "text to load"}, {"filter", "*.txt"}}});
    options.emplace_back<smeeze::wx::node_editor::node::option>(
        {"file_out",
         "",
         smeeze::wx::node_editor::node::option_type::file_save,
         {{"desc", "text to save"}, {"filter", "*.txt"}}});
    options.emplace_back<smeeze::wx::node_editor::node::option>(
        {"int0", "0", smeeze::wx::node_editor::node::option_type::int_});
    options.emplace_back<smeeze::wx::node_editor::node::option>(
        {"float0", "0.0", smeeze::wx::node_editor::node::option_type::float_});
    options.emplace_back<smeeze::wx::node_editor::node::option>(
        {"float1",
         "0.0",
         smeeze::wx::node_editor::node::option_type::float_,
         {{"max", "5"}}});
  }
  // functions
  std::vector<std::string> functions;
  if (s == "three") {
    functions.emplace_back("write");
  }
  const auto rv(_get_free_id());
  _nodes.emplace_back(smeeze::wx::node_editor::node(
      rv, s, {"in0", "in1"}, {"out0", "out1", "out2"}, options, functions));
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
void apps::node_editor::model::_node_del(const std::string &id) {
  std::experimental::erase_if(_connections, [&id](const auto &c) {
    return c.node0 == id || c.node1 == id;
  });
  std::experimental::erase_if(
      _nodes, [&id](const auto &n) { return n.get_id() == id; });
}
///////////////////////////////////////////////////////////////////////////////
void apps::node_editor::model::_node_rename(const std::string &from,
                                            const std::string &to) {
  fmt::print("node rename: {} -> {}\n", from, to);

  for (auto &n : _nodes) {
    if (n.get_id() == from) {
      n.set_id(to);
    }
  }

  for (auto &c : _connections) {
    if (c.node0 == from) {
      c.node0 = to;
    }
    if (c.node1 == from) {
      c.node1 = to;
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
void apps::node_editor::model::_node_inc_position(
    const std::string &id, const smeeze::geom::vec2<float> &p) {
  for (auto &n : _nodes) {
    if (n.get_id() == id) {
      n.inc_position(p);
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
void apps::node_editor::model::_node_set_position(
    const std::string &id, const smeeze::geom::vec2<float> &p) {
  for (auto &n : _nodes) {
    if (n.get_id() == id) {
      n.set_position(p);
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
const std::vector<smeeze::wx::node_editor::node> &
apps::node_editor::model::_get_nodes() const {
  return _nodes;
}
///////////////////////////////////////////////////////////////////////////////
void apps::node_editor::model::_node_to_back(const std::string &id) {
  for (size_t idx = 0; idx < _nodes.size(); ++idx) {
    const auto nw(_nodes.at(idx));
    if (nw.get_id() == id) {
      _nodes.erase(_nodes.begin() + idx);
      _nodes.push_back(nw);
      break;
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
std::string apps::node_editor::model::_node_info(const std::string &) {
  return "node info";
}
///////////////////////////////////////////////////////////////////////////////
bool apps::node_editor::model::_connection_is_valid(
    const smeeze::wx::node_editor::connection &c) const {
  if (c.node0 == c.node1) {
    return false;
  }
  return true;
}
///////////////////////////////////////////////////////////////////////////////
void apps::node_editor::model::_connection_add(
    const smeeze::wx::node_editor::connection &c) {
  _connections.emplace_back(c);
}
///////////////////////////////////////////////////////////////////////////////
void apps::node_editor::model::_connection_del(
    const smeeze::wx::node_editor::connection &c) {
  std::experimental::erase_if(_connections,
                              [&c](const auto &tmp) { return c == tmp; });
}
///////////////////////////////////////////////////////////////////////////////
const std::vector<smeeze::wx::node_editor::connection> &
apps::node_editor::model::_get_connections() const {
  return _connections;
}
///////////////////////////////////////////////////////////////////////////////
void apps::node_editor::model::_set_option(const std::string &node,
                                           const std::string &option,
                                           const std::string &value) {
  fmt::print("{}({}, {}, {})\n", __PRETTY_FUNCTION__, node, option, value);
}
///////////////////////////////////////////////////////////////////////////////
wxWindow *apps::node_editor::model::_get_output_window(
    wxWindow *parent, const std::string &node, const std::string &output_id) {
  fmt::print("{}({}, {})\n", __PRETTY_FUNCTION__, node, output_id);

  class window_wrapper : public wxStaticText {
  public:
    window_wrapper(wxWindow *parent, const std::string &id)
        : wxStaticText(parent, wxID_ANY, id) {
      // fmt::print("{}({})\n", __PRETTY_FUNCTION__, id);
    }
    ~window_wrapper() { fmt::print("{}()\n", __PRETTY_FUNCTION__); }
  };

  if (output_id == "out0") {
    return new window_wrapper(parent, "asdasd");
  }

  return nullptr;
}
///////////////////////////////////////////////////////////////////////////////
std::string apps::node_editor::model::_output_info(const std::string &,
                                                   const std::string &) {
  return "output info";
}
///////////////////////////////////////////////////////////////////////////////
void apps::node_editor::model::_clear() {
  _nodes.clear();
  _connections.clear();
}
///////////////////////////////////////////////////////////////////////////////
std::string apps::node_editor::model::_get_free_id() const {
  uint64_t counter(0);
  while (true) {
    const std::string candidate(fmt::format("node{:03}", counter));
    ++counter;
    bool found(false);
    for (const auto &n : _nodes) {
      if (n.get_id() == candidate) {
        found = true;
        break;
      }
    }
    if (!found) {
      return candidate;
    }
  }
  throw(std::runtime_error("should not get here"));
}
