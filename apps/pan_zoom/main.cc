// wx
#include <wx/wx.h>
// fmt
#include <fmt/core.h>
// smeeze wx
#include <smeeze/wx/pan_zoom/window.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
class my_render : public smeeze::wx::pan_zoom::render {
protected:
  void _go(wxGraphicsContext *gc) override {
    const auto c(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT));
    gc->SetFont(*wxSMALL_FONT, c);
    gc->SetPen(wxPen(c));

    for (int y = 0; y < 1000; y += 50) {
      for (int x = 0; x < 1000; x += 50) {
        gc->DrawText(fmt::format("{:4},{:4}", x, y), x, y);
      }
    }
  }
};
//
//
//
class my_frame : public wxFrame {
public:
  my_frame(wxWindow *parent = nullptr, int id = wxID_ANY,
           wxString title = "Demo", wxPoint pos = wxDefaultPosition,
           wxSize size = wxDefaultSize, int style = wxDEFAULT_FRAME_STYLE);

  void set_render(smeeze::wx::pan_zoom::render *r) { canvas->set_render(r); }

protected:
  smeeze::wx::pan_zoom::window *canvas;
};

my_frame::my_frame(wxWindow *parent, int id, wxString title, wxPoint pos,
                   wxSize size, int style)
    : wxFrame(parent, id, title, pos, size, style) {
  canvas = new smeeze::wx::pan_zoom::window(this);
}
//
//
//
class my_app : public wxApp {
public:
  void set_render(smeeze::wx::pan_zoom::render *r) { _my_frame->set_render(r); }

protected:
  bool OnInit() override {
    _my_frame = new my_frame();
    _my_frame->Show();
    return true;
  }

private:
  my_frame *_my_frame;
};
} // namespace
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
  my_render r;
  my_app *a(new my_app());
  wxApp::SetInstance(a);
  wxEntryStart(argc, argv);
  wxTheApp->CallOnInit();
  a->set_render(&r);
  wxTheApp->OnRun();
  wxTheApp->OnExit();
  wxEntryCleanup();
  return 0;
}
