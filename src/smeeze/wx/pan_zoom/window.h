#pragma once

// wx
#include <wx/graphics.h>
#include <wx/window.h>
// smeeze wx
#include <smeeze/wx/pan_zoom/render.h>
#include <smeeze/wx/visibility.h>

namespace smeeze {
namespace wx {
namespace pan_zoom {
class API_SMEEZE_WX window : public wxWindow {
public:
  window(wxWindow *parent, wxWindowID id = wxID_ANY,
         const wxPoint &pos = wxDefaultPosition,
         const wxSize &size = wxDefaultSize, long style = 0,
         const wxString &name = "PanAndZoomCanvas");

  void set_render(smeeze::wx::pan_zoom::render *);

private:
  void _draw_canvas(wxGraphicsContext *);

  void _on_paint(wxPaintEvent &);

  void _on_mouse_wheel(wxMouseEvent &);

  void _on_left_down(wxMouseEvent &);
  void _on_motion(wxMouseEvent &);
  void _on_left_up(wxMouseEvent &);
  void _on_capture_lost(wxMouseCaptureLostEvent &);

  void _pan_process(const wxPoint &, bool);
  void _pan_finish(bool);

private:
  int _zoom_factor;
  wxPoint2DDouble _pan_vector;
  wxPoint2DDouble _pan_vector_in_progress;
  wxPoint _pan_start;
  bool _pan_in_progress;

  smeeze::wx::pan_zoom::render *_render = nullptr;
};
} // namespace pan_zoom
} // namespace wx
} // namespace smeeze
