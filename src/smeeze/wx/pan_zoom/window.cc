// wx
#include <wx/dcbuffer.h>
// smeeze wx
#include <smeeze/wx/pan_zoom/window.h>
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::pan_zoom::window::window(wxWindow *parent, wxWindowID id,
                                     const wxPoint &pos, const wxSize &size,
                                     long style, const wxString &name)
    : wxWindow(parent, id, pos, size, style, name) {
  Bind(wxEVT_PAINT, &window::_on_paint, this);
  Bind(wxEVT_MOUSEWHEEL, &window::_on_mouse_wheel, this);
  Bind(wxEVT_LEFT_DOWN, &window::_on_left_down, this);

  SetBackgroundStyle(wxBG_STYLE_PAINT);

  _zoom_factor = 100;

  _pan_vector = wxPoint2DDouble(0, 0);
  _pan_start = wxPoint(0, 0);
  _pan_vector_in_progress = wxPoint2DDouble(0, 0);
  _pan_in_progress = false;
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::pan_zoom::window::set_render(smeeze::wx::pan_zoom::render *r) {
  _render = r;
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::pan_zoom::window::_draw_canvas(wxGraphicsContext *gc) {
  if (_render) {
    _render->go(gc);
  }
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::pan_zoom::window::_on_paint(wxPaintEvent &) {
  wxAutoBufferedPaintDC dc(this);
  dc.Clear();

  wxGraphicsContext *gc = wxGraphicsContext::Create(dc);

  if (gc) {
    double a = _zoom_factor / 100.0;
    wxPoint2DDouble totalPan = _pan_vector + _pan_vector_in_progress;

    gc->Translate(-totalPan.m_x, -totalPan.m_y);
    gc->Scale(a, a);

    _draw_canvas(gc);

    // delete gc;
  }
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::pan_zoom::window::_on_mouse_wheel(wxMouseEvent &event) {
  if (_pan_in_progress) {
    _pan_finish(false);
  }

  int rot = event.GetWheelRotation();
  int delta = event.GetWheelDelta();

  int oldZoom = _zoom_factor;
  _zoom_factor += 10 * (rot / delta);

  if (_zoom_factor < 10) {
    _zoom_factor = 10;
  }

  if (_zoom_factor > 800) {
    _zoom_factor = 800;
  }

  double a = oldZoom / 100.0;
  double b = _zoom_factor / 100.0;

  // Set the panVector so that the point below the cursor in the new
  // scaled/panned cooresponds to the same point that is currently below it.
  wxPoint2DDouble uvPoint = event.GetPosition();
  wxPoint2DDouble stPoint = uvPoint + _pan_vector;
  wxPoint2DDouble xypoint = stPoint / a;
  wxPoint2DDouble newSTPoint = b * xypoint;
  _pan_vector = newSTPoint - uvPoint;

  Refresh();
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::pan_zoom::window::_pan_process(const wxPoint &pt,
                                                bool refresh) {
  _pan_vector_in_progress = _pan_start - pt;

  if (refresh) {
    Refresh();
  }
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::pan_zoom::window::_pan_finish(bool refresh) {
  if (_pan_in_progress) {
    SetCursor(wxNullCursor);

    if (HasCapture()) {
      ReleaseMouse();
    }

    Unbind(wxEVT_LEFT_UP, &window::_on_left_up, this);
    Unbind(wxEVT_MOTION, &window::_on_motion, this);
    Unbind(wxEVT_MOUSE_CAPTURE_LOST, &window::_on_capture_lost, this);

    _pan_vector += _pan_vector_in_progress;
    _pan_vector_in_progress = wxPoint2DDouble(0, 0);
    _pan_in_progress = false;

    if (refresh) {
      Refresh();
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::pan_zoom::window::_on_left_down(wxMouseEvent &event) {
  wxCursor cursor(wxCURSOR_HAND);
  SetCursor(cursor);

  _pan_start = event.GetPosition();
  _pan_vector_in_progress = wxPoint2DDouble(0, 0);
  _pan_in_progress = true;

  Bind(wxEVT_LEFT_UP, &window::_on_left_up, this);
  Bind(wxEVT_MOTION, &window::_on_motion, this);
  Bind(wxEVT_MOUSE_CAPTURE_LOST, &window::_on_capture_lost, this);

  CaptureMouse();
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::pan_zoom::window::_on_motion(wxMouseEvent &event) {
  _pan_process(event.GetPosition(), true);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::pan_zoom::window::_on_left_up(wxMouseEvent &event) {
  _pan_process(event.GetPosition(), false);
  _pan_finish(true);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::pan_zoom::window::_on_capture_lost(wxMouseCaptureLostEvent &) {
  _pan_finish(true);
}
