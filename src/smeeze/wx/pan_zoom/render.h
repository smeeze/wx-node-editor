#pragma once

// wx
#include <wx/graphics.h>
// smeeze wx
#include <smeeze/wx/visibility.h>

namespace smeeze {
namespace wx {
namespace pan_zoom {
class API_SMEEZE_WX render {
public:
  virtual ~render() {}

  void go(wxGraphicsContext *gc) { _go(gc); }

protected:
  virtual void _go(wxGraphicsContext *) = 0;
};
} // namespace pan_zoom
} // namespace wx
} // namespace smeeze
