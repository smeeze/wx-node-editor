#pragma once

#ifdef _WIN32
#define API_SMEEZE_WX_EXPORT_BASE __declspec(dllexport)
#define API_SMEEZE_WX_IMPORT_BASE
#else
#define API_SMEEZE_WX_EXPORT_BASE __attribute__((visibility("default")))
#define API_SMEEZE_WX_IMPORT_BASE
#endif

#ifdef SMEEZE_WX_EXPORT
#define API_SMEEZE_WX API_SMEEZE_WX_EXPORT_BASE
#else
#define API_SMEEZE_WX API_SMEEZE_WX_IMPORT_BASE
#endif
