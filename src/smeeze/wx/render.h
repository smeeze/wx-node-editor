#pragma once

// wx
#include <wx/wx.h>
// smeeze geom
#include <smeeze/geom/line.h>
#include <smeeze/geom/rect.h>
#include <smeeze/wx/node_editor/node.h>

namespace smeeze {
namespace wx {
class render {
public:
  render(wxDC &);

  wxColour grid_color;
  wxColour active_color;
  wxColour foreground_color;
  wxColour background_color;
  wxColour green_color;
  wxColour red_color;

  void set_pen(const wxPen &);
  void set_brush(const wxBrush &);
  void draw_line(float, float, float, float);
  void draw_line(const smeeze::geom::vec2<float> &,
                 const smeeze::geom::vec2<float> &);
  void draw_line(const smeeze::geom::line &);
  void draw_rectangle(const smeeze::geom::rect &);
  void draw_rectangle(const smeeze::geom::rect &, float);
  void draw_text(const std::string &, const smeeze::geom::vec2<float> &);
  void draw_circle(const smeeze::geom::vec2<float> &, float);
  wxSize get_text_extent(const std::string &);

  void draw_connection(const smeeze::geom::vec2<float> &,
                       const smeeze::geom::vec2<float> &);
  void draw_connection(const smeeze::geom::rect &,
                       const smeeze::geom ::vec2<float> &);
  void draw_connection(const smeeze::geom::vec2<float> &,
                       const smeeze::geom::rect &);
  void draw_connection(const smeeze::geom::rect &, const smeeze::geom::rect &);

  void draw_node(const smeeze::wx::node_editor::node &, bool);

protected:
  wxDC &_dc;
};
} // namespace wx
} // namespace smeeze
