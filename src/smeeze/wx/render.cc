// smeeze wx
#include <smeeze/geom/utils.h>
#include <smeeze/wx/render.h>
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::render::render(wxDC &dc) : _dc(dc) {
  grid_color = wxSystemSettings::GetColour(wxSYS_COLOUR_GRAYTEXT);
  active_color = wxSystemSettings::GetColour(wxSYS_COLOUR_HOTLIGHT);
  foreground_color = wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOWTEXT);
  background_color = wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW);
  green_color = *wxGREEN;
  red_color = *wxRED;
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::set_pen(const wxPen &p) { _dc.SetPen(p); }
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::set_brush(const wxBrush &p) { _dc.SetBrush(p); }
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_line(float x0, float y0, float x1, float y1) {
  _dc.DrawLine(x0, y0, x1, y1);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_line(const smeeze::geom::vec2<float> &p0,
                                   const smeeze::geom::vec2<float> &p1) {
  draw_line(p0.x(), p0.y(), p1.x(), p1.y());
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_line(const geom::line &l) {
  draw_line(l.p0, l.p1);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_rectangle(const geom::rect &rect) {
  _dc.DrawRectangle(rect.x(), rect.y(), rect.w(), rect.h());
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_rectangle(const geom::rect &rect, float radius) {
  _dc.DrawRoundedRectangle(rect.x(), rect.y(), rect.w(), rect.h(), radius);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_text(const std::string &msg,
                                   const smeeze::geom::vec2<float> &p) {
  _dc.DrawText(msg, p.x(), p.y());
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_circle(const smeeze::geom::vec2<float> &p,
                                     float radius) {
  _dc.DrawCircle(p.x(), p.y(), radius);
}
///////////////////////////////////////////////////////////////////////////////
wxSize smeeze::wx::render::get_text_extent(const std::string &msg) {
  return _dc.GetTextExtent(msg);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_connection(const smeeze::geom::vec2<float> &a,
                                         const smeeze::geom::vec2<float> &b) {
  for (const auto &l : smeeze::geom::utils::interpolate(a, b, 32)) {
    draw_line(l);
  }
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_connection(const geom::rect &a,
                                         const smeeze::geom::vec2<float> &b) {
  draw_connection(a.get_center(), b);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_connection(const smeeze::geom::vec2<float> &a,
                                         const geom::rect &b) {
  draw_connection(a, b.get_center());
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_connection(const geom::rect &a,
                                         const geom::rect &b) {
  draw_connection(a.get_center(), b.get_center());
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::render::draw_node(const smeeze::wx::node_editor::node &nw,
                                   bool is_selected) {
  set_pen(wxPen(foreground_color));
  set_brush(wxBrush(background_color));
  if (is_selected) {
    set_pen(wxPen(active_color, 2));
  }

  // outline
  draw_rectangle(nw.get_rect(), nw.get_radius());

  set_brush(wxBrush(background_color, wxBRUSHSTYLE_TRANSPARENT));

  // contents
  const auto node_position(nw.get_position());
  draw_text(nw.get_name(), node_position + smeeze::geom::vec2<float>(5, 5));
  draw_line(node_position + nw.get_body_offset(),
            node_position + nw.get_width_offset() + nw.get_body_offset());

  for (const auto &c : nw.get_inputs()) {
    const auto center(c.rect.get_center());
    draw_circle(center, nw.get_radius());
    const auto e(get_text_extent(c.id));
    draw_text(c.id, center + smeeze::geom::vec2<float>(nw.get_radius(),
                                                       -e.GetHeight() / 2));
  }
  for (const auto &c : nw.get_outputs()) {
    const auto center(c.rect.get_center());
    draw_circle(center, nw.get_radius());
    const auto e(get_text_extent(c.id));
    draw_text(c.id, center + smeeze::geom::vec2<float>(-e.GetWidth() -
                                                           nw.get_radius(),
                                                       -e.GetHeight() / 2));
  }
}
