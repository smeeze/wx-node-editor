#pragma once

// system
#include <string>
// smeeze wx
#include <smeeze/wx/visibility.h>

namespace smeeze {
namespace wx {
namespace node_editor {
class API_SMEEZE_WX connection {
public:
  std::string node0;
  std::string output;
  std::string node1;
  std::string input;

  bool operator==(const connection &rhs) const;
};
} // namespace node_editor
} // namespace wx
} // namespace smeeze
