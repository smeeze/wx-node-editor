// wx
#include <wx/dialog.h>
#include <wx/filepicker.h>
#include <wx/textctrl.h>
#include <wx/textdlg.h>
// smeeze wx
#include <smeeze/wx/node_editor/menus/node.h>
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::menus::node::node(wxWindow *win, model *model,
                                           const std::string &node_id) {
  int menu_id(0);

  auto n(model->node_find(node_id));

  Append(menu_id, n.get_id());
  ++menu_id;

  // misc
  AppendSeparator();
  Append(menu_id, "remove");
  this->Bind(
      wxEVT_MENU,
      [this, model, node_id](wxCommandEvent &) {
        model->node_del(node_id);
        _has_modified_model = true;
      },
      menu_id);
  ++menu_id;
  Append(menu_id, "rename");
  this->Bind(
      wxEVT_MENU,
      [this, win, model, node_id](wxCommandEvent &) {
        // TODO
        wxTextEntryDialog dlg(win, "edit", "rename node", node_id);
        if (dlg.ShowModal() == wxID_OK) {
          model->node_rename(node_id, dlg.GetValue().ToStdString());
          _has_modified_model = true;
        }
      },
      menu_id);
  ++menu_id;

  // options
  if (n.get_options().size()) {
    AppendSeparator();
    wxMenu *options(new wxMenu);
    AppendSubMenu(options, "options");
    for (const auto &o : n.get_options()) {
      options->Append(menu_id, o.id);
      this->Bind(
          wxEVT_MENU,
          [this, win, model, node_id, o](wxCommandEvent &) {
            switch (o.type) {
            case smeeze::wx::node_editor::node::option_type::int_:
              // TODO
            case smeeze::wx::node_editor::node::option_type::float_:
              // TODO
            case smeeze::wx::node_editor::node::option_type::string: {
              wxTextEntryDialog dlg(win, "edit", o.id, o.value);
              if (dlg.ShowModal() == wxID_OK) {
                model->set_option(node_id, o.id, dlg.GetValue().ToStdString());
                _has_modified_model = true;
              }
            } break;
            case smeeze::wx::node_editor::node::option_type::file_load: {
              wxFileDialog openFileDialog(win, o.get_param("desc", ""), "", "",
                                          o.get_param("filter", ".*"),
                                          wxFD_OPEN | wxFD_FILE_MUST_EXIST |
                                              wxFLP_SMALL);
              if (openFileDialog.ShowModal() == wxID_CANCEL) {
              } else {
                model->set_option(node_id, o.id,
                                  openFileDialog.GetPath().ToStdString());
                _has_modified_model = true;
              }
            } break;
            case smeeze::wx::node_editor::node::option_type::file_save: {
              wxFileDialog openFileDialog(win, o.get_param("desc", ""), "", "",
                                          o.get_param("filter", ".*"),
                                          wxFD_SAVE | wxFD_OVERWRITE_PROMPT |
                                              wxFLP_SMALL);
              if (openFileDialog.ShowModal() == wxID_CANCEL) {
              } else {
                model->set_option(node_id, o.id,
                                  openFileDialog.GetPath().ToStdString());
                _has_modified_model = true;
              }
            } break;
            }
          },
          menu_id);
      ++menu_id;
    }
  }

  // functions
  if (n.get_functions().size()) {
    AppendSeparator();
    wxMenu *functions(new wxMenu);
    AppendSubMenu(functions, "functions");
    for (const auto &f : n.get_functions()) {
      functions->Append(menu_id, f);
      this->Bind(
          wxEVT_MENU,
          [model, node_id, f](wxCommandEvent &) {
            model->call_function(node_id, f);
          },
          menu_id);
      ++menu_id;
    }
  }
}
