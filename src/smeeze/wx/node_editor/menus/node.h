#pragma once

// smeeze wx
#include <smeeze/wx/node_editor/menus/base.h>
#include <smeeze/wx/node_editor/model.h>

namespace smeeze {
namespace wx {
namespace node_editor {
namespace menus {
class node : public smeeze::wx::node_editor::menus::base {
public:
  node(wxWindow *, smeeze::wx::node_editor::model *, const std::string &);
};
} // namespace menus
} // namespace node_editor
} // namespace wx
} // namespace smeeze
