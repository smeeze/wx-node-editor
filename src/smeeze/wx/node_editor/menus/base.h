#pragma once

// wx
#include <wx/menu.h>

namespace smeeze {
namespace wx {
namespace node_editor {
namespace menus {
class base : public wxMenu {
public:
  ~base();

  bool has_modified_model() const { return _has_modified_model; }
  bool is_valid() const { return _is_valid; }

protected:
  bool _has_modified_model = false;
  bool _is_valid = true;
};
}}}}
