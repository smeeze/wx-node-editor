#pragma once

// smeeze wx
#include <smeeze/wx/node_editor/menus/base.h>
#include <smeeze/wx/node_editor/model.h>

namespace smeeze {
namespace wx {
namespace node_editor {
namespace menus {
class connection : public smeeze::wx::node_editor::menus::base {
public:
  connection(smeeze::wx::node_editor::model *,
             const smeeze::wx::node_editor::connection &);
};
} // namespace menus
} // namespace node_editor
} // namespace wx
} // namespace smeeze
