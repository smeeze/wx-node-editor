// smeeze wx
#include <smeeze/wx/node_editor/menus/connection.h>
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::menus::connection::connection(
    model *model, const smeeze::wx::node_editor::connection &con) {
  int menu_id(0);

  Append(menu_id, "connection");
  ++menu_id;

  AppendSeparator();
  Append(menu_id, "remove");
  this->Bind(
      wxEVT_MENU,
      [this, model, con](wxCommandEvent &) {
        model->connection_del(con);
        _has_modified_model = true;
      },
      menu_id);
  ++menu_id;
}
