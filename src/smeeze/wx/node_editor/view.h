#pragma once

// wx
#include <wx/panel.h>
#include <wx/statusbr.h>
// smeeze wx
#include <smeeze/geom/line.h>
#include <smeeze/wx/node_editor/model.h>
#include <smeeze/wx/visibility.h>
///////////////////////////////////////////////////////////////////////////////
namespace smeeze {
namespace wx {
namespace node_editor {
class API_SMEEZE_WX view : public wxPanel {
public:
  view(wxPanel *, wxStatusBar *, smeeze::wx::node_editor::model *);

  void recompute();

private:
  void _on_paint(wxPaintEvent &);
  void _mouse_button_left_down(wxMouseEvent &);
  void _mouse_button_left_up(wxMouseEvent &);
  void _mouse_button_right_down(wxMouseEvent &);
  void _mouse_motion(wxMouseEvent &);

  void _process(const std::string &, const std::function<void()> &) const;
  wxStatusBar *_status_bar;

private:
  smeeze::wx::node_editor::model *_model;
  int _grid_div_x = 64;
  int _grid_div_y = 64;

  struct pc_connection {
    smeeze::wx::node_editor::connection con;
    std::vector<smeeze::geom::line> lines;
  };
  std::vector<pc_connection> _precomputed_connections;

private:
  struct pick {
    enum class mode_t { none, node, input, output, connection };
    mode_t mode = mode_t::none;

    std::string node_id;
    std::string input_id;
    std::string output_id;

    smeeze::geom::vec2<float> position;
    smeeze::geom::rect connector_rect;
    smeeze::wx::node_editor::connection connection;
  };
  pick _pick(const smeeze::geom::vec2<float> &) const;
  bool _is_valid_connection(const pick &, const pick &) const;
  bool _is_selected(const smeeze::wx::node_editor::node &,
                    const smeeze::wx::node_editor::view::pick &);
  bool _is_selected(const smeeze::wx::node_editor::connection &,
                    const smeeze::wx::node_editor::view::pick &);

private:
  struct mouse_context {
    pick left_down_pick;
    pick right_down_pick;
    pick motion_pick;

    bool connection_is_valid = false;
  };
  mouse_context _mouse_context;
};
} // namespace node_editor
} // namespace wx
} // namespace smeeze
