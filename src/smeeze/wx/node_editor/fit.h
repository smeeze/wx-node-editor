#pragma once

// system
#include <cassert>
#include <cmath>

#include <algorithm>
#include <array>
#include <string>
#include <vector>
// smeeze wx

namespace smeeze {
namespace wx {
namespace node_editor {
namespace geom {
struct point {
  point(float, float);

  const float &operator[](size_t) const;

  point &operator-=(const point &);
  point operator-(const point &) const;
  point &operator+=(const point &);
  point operator+(const point &) const;
  point &operator*=(const float &);
  point operator*(const float &) const;
  float length() const;

  std::array<float, 2> data;
};
//
//
//
#if 0
inline bool ccw(const point &a, const point &b, const point &c) {
  return (c[1] - a[1]) * (b[0] - a[0]) > (b[1] - a[1]) * (c[0] - a[0]);
}
//
//
//
inline bool intersect(const point &a, const point &b, const point &c,
                      const point &d) {
  return ccw(a, c, d) != ccw(b, c, d) && ccw(a, b, c) != ccw(a, b, d);
}
#endif
} // namespace geom
class fit {
public:
  //
  //
  //
  struct connection {
    std::string left;
    std::string right;

    bool operator==(const connection &c) const {
      return left == c.left && right == c.right;
    }
  };
  //
  //
  //
  void add_connection(const connection &c);
  //
  //
  //
  struct placement {
    uint32_t rank = 0;
    uint32_t order = 0;
    std::string node;
  };
  //
  //
  //
  struct solution {
    std::vector<placement> fixed_nodes;
    uint32_t crossings = 0;
  };
  //
  //
  //
  solution solve() const;

private:
  //
  //
  //
  std::vector<solution> _solve() const;
  //
  //
  //
  void _solve_recursive(const solution &, std::vector<solution> &, uint32_t,
                        uint32_t) const;
  //
  //
  //
  uint32_t _get_rank(const std::string &) const;
  //
  //
  //
  uint32_t _get_max_rank() const;
  //
  //
  //
  std::vector<std::string> _get_leaders(const std::string &) const;
  //
  //
  //
  std::vector<std::string> _get_nodes() const;
  //
  //
  //
  bool _has_connection(const connection &) const;
  //
  //
  //
  void _count_crossings(solution &) const;

private:
  std::vector<connection> _connections;
};
} // namespace node_editor
} // namespace wx
} // namespace smeeze
