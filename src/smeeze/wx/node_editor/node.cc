// system
#include <stdexcept>
// smeeze wx
#include <smeeze/geom/utils.h>
#include <smeeze/wx/node_editor/node.h>
///////////////////////////////////////////////////////////////////////////////
int smeeze::wx::node_editor::node::get_radius() const { return _radius; }
///////////////////////////////////////////////////////////////////////////////
smeeze::geom::vec2<float> smeeze::wx::node_editor::node::get_position() const {
  return _position.snap(16);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::node::inc_position(
    const smeeze::geom::vec2<float> &p) {
  _position += p;
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::node::set_position(
    const smeeze::geom::vec2<float> &p) {
  _position = p;
}
///////////////////////////////////////////////////////////////////////////////
const std::string &smeeze::wx::node_editor::node::get_id() const { return _id; }
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::node::set_id(const std::string &s) { _id = s; }
///////////////////////////////////////////////////////////////////////////////
const std::string &smeeze::wx::node_editor::node::get_name() const {
  return _name;
}
///////////////////////////////////////////////////////////////////////////////
std::vector<smeeze::wx::node_editor::node::connector>
smeeze::wx::node_editor::node::get_inputs() const {
  std::vector<smeeze::wx::node_editor::node::connector> rv;

  int n(0);
  for (const auto &id : _inputs) {
    const smeeze::geom::vec2<float> p0{0.f, (n + 1) * 2.f * _space_y};
    const smeeze::geom::vec2<float> size{2.f * _space_x, 2.f * _space_y};
    rv.emplace_back<smeeze::wx::node_editor::node::connector>(
        {id, {get_position() + p0, get_position() + p0 + size}});
    ++n;
  }
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::node::connector
smeeze::wx::node_editor::node::get_input_by_id(const std::string &id) const {
  for (const auto &c : get_inputs()) {
    if (c.id == id) {
      return c;
    }
  }
  throw(std::runtime_error("could not find input by id"));
}
///////////////////////////////////////////////////////////////////////////////
std::vector<smeeze::wx::node_editor::node::connector>
smeeze::wx::node_editor::node::get_outputs() const {
  std::vector<smeeze::wx::node_editor::node::connector> rv;

  int n(0);
  for (const auto &id : _outputs) {
    const smeeze::geom::vec2<float> p0{(_space_x_count - 2.f) * _space_x,
                                       (n + 1) * 2.f * _space_y};
    const smeeze::geom::vec2<float> size{2.f * _space_x, 2.f * _space_y};
    rv.emplace_back<smeeze::wx::node_editor::node::connector>(
        {id, {get_position() + p0, get_position() + p0 + size}});
    ++n;
  }
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::node::connector
smeeze::wx::node_editor::node::get_output_by_id(const std::string &id) const {
  for (const auto &c : get_outputs()) {
    if (c.id == id) {
      return c;
    }
  }
  throw(std::runtime_error("could not find output by id"));
}
///////////////////////////////////////////////////////////////////////////////
const std::vector<smeeze::wx::node_editor::node::option> &
smeeze::wx::node_editor::node::get_options() const {
  return _options;
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::node::option &
smeeze::wx::node_editor::node::get_option_by_id(const std::string &id) {
  for (auto &c : _options) {
    if (c.id == id) {
      return c;
    }
  }
  throw(std::runtime_error("could not find option by id"));
}
///////////////////////////////////////////////////////////////////////////////
smeeze::geom::rect smeeze::wx::node_editor::node::get_rect() const {
  return {get_position(),
          get_position() + smeeze::geom::vec2<float>{_get_width(),
                                                     _get_height() + _padding}};
}
///////////////////////////////////////////////////////////////////////////////
bool smeeze::wx::node_editor::node::intersects(
    const smeeze::geom::line &l_in, float &f, smeeze::geom::vec2<float> &i,
    smeeze::geom::line &l_out) const {
  const auto r(get_rect());
  const std::vector<smeeze::geom::line> edges{
      smeeze::geom::line{r.get_top_left(), r.get_top_right()},
      smeeze::geom::line{r.get_top_right(), r.get_bottom_right()},
      smeeze::geom::line{r.get_bottom_right(), r.get_bottom_left()},
      smeeze::geom::line{r.get_bottom_left(), r.get_top_left()},
  };

  bool rv(false);
  f = 1e6;
  for (const auto &o : edges) {
    float f_tmp;
    smeeze::geom::vec2<float> i_tmp;
    if (l_in.intersects(o, f_tmp, i_tmp) && (f_tmp < f)) {
      rv = true;
      f = f_tmp;
      i = i_tmp;
      l_out = o;
    }
  }
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
smeeze::geom::vec2<float>
smeeze::wx::node_editor::node::get_width_offset() const {
  return smeeze::geom::vec2<float>{_get_width(), 0};
}
///////////////////////////////////////////////////////////////////////////////
smeeze::geom::vec2<float>
smeeze::wx::node_editor::node::get_body_offset() const {
  return smeeze::geom::vec2<float>{0, _space_y * 2};
}
///////////////////////////////////////////////////////////////////////////////
float smeeze::wx::node_editor::node::_get_width() const {
  return _space_x_count * _space_x;
}
///////////////////////////////////////////////////////////////////////////////
float smeeze::wx::node_editor::node::_get_height() const {
  return _space_y * 2 +
         std::max(_inputs.size(), _outputs.size()) * 2 * _space_y;
}
