// system
#include <iostream>
#include <regex>
// wx
#include <wx/dnd.h>
// smeeze wx
#include <smeeze/wx/node_editor/control.h>
#include <smeeze/wx/node_editor/fit.h>
#include <smeeze/wx/node_editor/view.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
class _control_drop : public wxTextDropTarget {
public:
  _control_drop(smeeze::wx::node_editor::control *control)
      : _control(control) {}

private:
  bool OnDropText(wxCoord x, wxCoord y, const wxString &t) override {
    _control->handle_drop(x, y, t.ToStdString());
    return true;
  }

private:
  smeeze::wx::node_editor::control *_control;
};
} // namespace
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::control::control(model *model, wxWindow *parent,
                                          wxWindowID id, const wxString &title)
    : wxFrame(parent, id, title), _model(model) {
  SetSize(wxSize(800, 600));
  SetTitle(title);

  //
  _status_bar = CreateStatusBar();

  // item list control
  _item_list_ctrl = new wxListCtrl(this, wxID_ANY, wxDefaultPosition,
                                   wxDefaultSize, wxLC_REPORT);
  _item_list_ctrl->Arrange(wxLIST_ALIGN_LEFT);
  _item_list_ctrl->InsertColumn(0, "nodes");

  // item list selector
  auto item_list_selector = new wxTextCtrl(this, wxID_ANY, wxEmptyString);
  item_list_selector->AppendText(".*");

  // push buttons
  auto button_load = new wxButton(this, wxID_ANY, "load");
  auto button_save = new wxButton(this, wxID_ANY, "save");
  auto button_fit = new wxButton(this, wxID_ANY, "fit");
  auto button_func = new wxButton(this, wxID_ANY, "func");
  auto button_clear = new wxButton(this, wxID_ANY, "clear");
  auto button_quit = new wxButton(this, wxID_ANY, "quit");

  // state view
  wxScrolledWindow *sw = new wxScrolledWindow(this);
  _view = new smeeze::wx::node_editor::view(sw, _status_bar, _model);
  _view->SetSize(4096, 4096);
  _view->SetDropTarget(new _control_drop(this));

  sw->SetScrollbars(1, 1, _view->GetSize().GetX(), _view->GetSize().GetY());

  // sizers
  auto sizer_top = new wxBoxSizer(wxHORIZONTAL);
  SetSizer(sizer_top);

  auto sizer_left = new wxBoxSizer(wxVERTICAL);
  sizer_top->Add(sizer_left, 1, wxEXPAND);

  auto sizer_right = new wxBoxSizer(wxVERTICAL);
  sizer_top->Add(sizer_right, 10, wxEXPAND);

  wxGridSizer *sizer_grid_buttons = new wxGridSizer(3, 2, 0, 0);

  sizer_left->Add(_item_list_ctrl, 10, wxEXPAND);
  sizer_left->Add(item_list_selector, 0, wxEXPAND);
  sizer_left->Add(sizer_grid_buttons, 0);
  sizer_grid_buttons->Add(button_load);
  sizer_grid_buttons->Add(button_save);
  sizer_grid_buttons->Add(button_fit);
  sizer_grid_buttons->Add(button_func);
  sizer_grid_buttons->Add(button_clear);
  sizer_grid_buttons->Add(button_quit);
  sizer_right->Add(sw, 10, wxEXPAND);

  // set defaults
  _set_list_selection(".*");

  //
  Layout();

  // bind
  item_list_selector->Bind(wxEVT_TEXT, [this](wxCommandEvent &event) {
    this->_set_list_selection(event.GetString().ToStdString());
  });
  button_load->Bind(wxEVT_BUTTON, [this](wxCommandEvent &) {
    _process("load", [&]() {
      wxFileDialog openFileDialog(this, _("Open file"), "", "",
                                  _model->get_filename_extension(),
                                  // "XYZ files (*.xyz)|*.xyz",
                                  wxFD_OPEN | wxFD_FILE_MUST_EXIST);
      if (openFileDialog.ShowModal() == wxID_CANCEL) {
        return;
      }
      _model->load(openFileDialog.GetPath().ToStdString());
      _view->recompute();
      _view->Refresh();
    });
  });
  button_save->Bind(wxEVT_BUTTON, [this](wxCommandEvent &) {
    _process("save", [&]() {
      wxFileDialog saveFileDialog(this, _("Save file"), "", "",
                                  _model->get_filename_extension(),
                                  // "XYZ files (*.xyz)|*.xyz",
                                  wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
      if (saveFileDialog.ShowModal() == wxID_CANCEL) {
        return;
      }
      _model->save(saveFileDialog.GetPath().ToStdString());
    });
  });
  button_fit->Bind(wxEVT_BUTTON, [this](wxCommandEvent &) {
    _process("fit", [&]() {
      smeeze::wx::node_editor::fit f;
      for (const auto &c : _model->get_connections()) {
        f.add_connection({c.node0, c.node1});
      }

      const auto solution(f.solve());
      for (const auto &p : solution.fixed_nodes) {
        _model->node_set_position(p.node, {
                                              16.f + (200 * p.rank),
                                              16.f + (64 * p.order),
                                          });
      }

      _view->recompute();
      ;
      _view->Refresh();
    });
  });
  button_func->Bind(wxEVT_BUTTON, [this](wxCommandEvent &) {
    _process("func", [&]() {
      for (const auto &n : _model->get_nodes()) {
        for (const auto &f : n.get_functions()) {
          _model->call_function(n.get_id(), f);
        }
      }
      _view->Refresh();
    });
  });
  button_clear->Bind(wxEVT_BUTTON, [this](wxCommandEvent &) {
    _process("clear", [&]() {
      _model->clear();
      _view->recompute();
      ;
      _view->Refresh();
    });
  });
  button_quit->Bind(wxEVT_BUTTON, [this](wxCommandEvent &) { this->Close(); });

  _item_list_ctrl->Bind(wxEVT_LIST_BEGIN_DRAG, [this](wxListEvent &event) {
    wxTextDataObject tdo(event.GetItem().GetText());
    wxDropSource tds(tdo, _item_list_ctrl);
    tds.DoDragDrop(wxDrag_CopyOnly);
  });

  _view->Bind(wxEVT_MOVE, [sw, this](wxMoveEvent &e) {
    const wxPoint p(sw->GetScrollPos(wxHORIZONTAL),
                    sw->GetScrollPos(wxVERTICAL));
    const wxPoint np(p - e.GetPosition());
    _view->SetEvtHandlerEnabled(false);
    sw->Scroll(np.x, np.y);
    _view->SetEvtHandlerEnabled(true);
  });
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::control::handle_drop(int x, int y,
                                                   const std::string &t) {
  _process("dropping", [this, &x, &y, &t]() {
    _model->node_set_position(_model->node_add(t), {(float)x, (float)y});
    _view->recompute();
  });
  Refresh();
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::control::_set_list_selection(
    const std::string &sel) {
  _process("set listbox selection", [this, &sel]() {
    wxArrayString as;
    auto selection(_model->node_list_available());
    _item_list_ctrl->DeleteAllItems();
    int id(0);
    std::regex re(sel);
    for (const auto &i : selection) {
      std::smatch matches;
      std::string s(i);
      if (std::regex_search(s, matches, re)) {
        wxListItem item;
        item.SetColumn(0);
        item.SetId(id);
        item.SetText(s);

        _item_list_ctrl->InsertItem(item);
        ++id;
      }
    }
    _item_list_ctrl->SetColumnWidth(0, wxLIST_AUTOSIZE);
  });
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::control::_process(
    const std::string &msg, const std::function<void()> &f) {
  try {
    _status_bar->SetStatusText(msg);
    f();
    _status_bar->SetStatusText("...");
  } catch (const std::exception &e) {
    std::cerr << msg << ": error: " << e.what() << std::endl;
    _status_bar->SetStatusText(e.what());
  }
}
