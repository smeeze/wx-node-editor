// system
#include <array>
#include <iostream>
#include <stack>
// wx
#include <wx/dcbuffer.h>
#include <wx/dnd.h>
#include <wx/settings.h>
#include <wx/wx.h>
// smeeze wx
#include <smeeze/geom/utils.h>
#include <smeeze/wx/node_editor/dialogs/output.h>
#include <smeeze/wx/node_editor/menus/connection.h>
#include <smeeze/wx/node_editor/menus/node.h>
#include <smeeze/wx/node_editor/view.h>
#include <smeeze/wx/render.h>
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::view::view(wxPanel *parent, wxStatusBar *status_bar,
                                    smeeze::wx::node_editor::model *model)
    : wxPanel(parent), _status_bar(status_bar), _model(model) {
  Bind(wxEVT_PAINT, &view::_on_paint, this);
  Bind(wxEVT_LEFT_DOWN, &view::_mouse_button_left_down, this);
  Bind(wxEVT_LEFT_UP, &view::_mouse_button_left_up, this);
  Bind(wxEVT_RIGHT_DOWN, &view::_mouse_button_right_down, this);
  Bind(wxEVT_MOTION, &view::_mouse_motion, this);

  SetDoubleBuffered(true);

  recompute();
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::view::recompute() {
  _precomputed_connections.clear();
  for (const auto &c : _model->get_connections()) {
    const auto b0(_model->node_find(c.node0));
    const auto b1(_model->node_find(c.node1));
    const auto p0(b0.get_output_by_id(c.output).rect.get_center());
    const auto p1(b1.get_input_by_id(c.input).rect.get_center());
    _precomputed_connections.emplace_back(pc_connection {
      c, //
#if 0
          smeeze::geom::path::solve(p0, p1, _model->get_nodes())
#endif
#if 0
        {smeeze::geom::line{p0 + wxPoint(16, 0), p1 - wxPoint(16, 0)}}
#endif
#if 1
          smeeze::geom::utils::interpolate(p0, p1, 32)
#endif
    });
  }
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::view::_on_paint(wxPaintEvent &) {
  wxPaintDC dc(this);
  smeeze::wx::render r(dc);
  // lines
  {
    int grid_width;
    int grid_height;
    GetSize(&grid_width, &grid_height);
    r.set_pen(wxPen(r.grid_color));
    for (int x = 0; x <= grid_width; x += _grid_div_x) {
      r.draw_line(x, 0, x, grid_height);
    }
    for (int y = 0; y <= grid_height; y += _grid_div_y) {
      r.draw_line(0, y, grid_width, y);
    }
  }

  _process("paint", [&]() {
    // nodes
    for (const auto &nw : _model->get_nodes()) {
      r.draw_node(nw, _is_selected(nw, _mouse_context.motion_pick));
    }
    // connections
    for (const auto &c : _precomputed_connections) {
      if (_is_selected(c.con, _mouse_context.motion_pick)) {
        r.set_pen(wxPen(r.active_color, 4));
      } else {
        r.set_pen(wxPen(r.foreground_color, 2));
      }
      for (const auto &l : c.lines) {
        r.draw_line(l);
      }
    }
    // intersections
    if (false) {
      r.set_pen(wxPen(r.red_color, 2));
      for (const auto &c : _precomputed_connections) {
        for (const auto &l0 : c.lines) {
          for (const auto &n : _model->get_nodes()) {
            float f;
            smeeze::geom::line l1;
            smeeze::geom::vec2<float> i;
            if (n.intersects(l0, f, i, l1)) {
              r.draw_circle(i, 4);
              r.draw_line(l1);
            }
          }
        }
      }
    }
  });
  if (_mouse_context.connection_is_valid) {
    r.set_pen(wxPen(r.green_color, 2));
  } else {
    r.set_pen(wxPen(r.red_color, 2));
  }
  switch (_mouse_context.left_down_pick.mode) {
  case smeeze::wx::node_editor::view::pick::mode_t::input:
    if (_mouse_context.motion_pick.mode ==
        smeeze::wx::node_editor::view::pick::mode_t::output) {
      r.draw_connection(_mouse_context.motion_pick.connector_rect,
                        _mouse_context.left_down_pick.connector_rect);
    } else {
      r.draw_connection(_mouse_context.motion_pick.position,
                        _mouse_context.left_down_pick.connector_rect);
    }
    break;
  case smeeze::wx::node_editor::view::pick::mode_t::output:
    if (_mouse_context.motion_pick.mode ==
        smeeze::wx::node_editor::view::pick::mode_t::input) {
      r.draw_connection(_mouse_context.left_down_pick.connector_rect,
                        _mouse_context.motion_pick.connector_rect);
    } else {
      r.draw_connection(_mouse_context.left_down_pick.connector_rect,
                        _mouse_context.motion_pick.position);
    }
    break;
  default:
    break;
  }
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::view::_mouse_button_left_down(
    wxMouseEvent &event) {
  const smeeze::geom::vec2<float> position(event.GetPosition().x,
                                           event.GetPosition().y);
  _mouse_context.left_down_pick = _pick(position);

  switch (_mouse_context.left_down_pick.mode) {
  case smeeze::wx::node_editor::view::pick::mode_t::node:
    _process("left click", [&]() {
      _model->node_to_back(_mouse_context.left_down_pick.node_id);
      // recompute();
      ;
    });
    break;
  default:
    break;
  }
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::view::_mouse_button_left_up(wxMouseEvent &event) {
  const smeeze::geom::vec2<float> position(event.GetPosition().x,
                                           event.GetPosition().y);

  const auto connection_add([this](const pick &p0, const pick &p1) {
    _process("connection add", [&]() {
      const connection c{p0.node_id, p0.output_id, p1.node_id, p1.input_id};
      _model->connection_add(c);
      recompute();
      ;
    });
  });

  switch (_mouse_context.left_down_pick.mode) {
  case smeeze::wx::node_editor::view::pick::mode_t::input:
    if (_mouse_context.connection_is_valid) {
      connection_add(_pick(position), _mouse_context.left_down_pick);
    }
    break;
  case smeeze::wx::node_editor::view::pick::mode_t::output:
    if (_mouse_context.connection_is_valid) {
      connection_add(_mouse_context.left_down_pick, _pick(position));
    }
    break;
  default:
    break;
  }

  _mouse_context.left_down_pick.mode = pick::mode_t::none;
  Refresh();
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::view::_mouse_button_right_down(
    wxMouseEvent &event) {
  const smeeze::geom::vec2<float> position(event.GetPosition().x,
                                           event.GetPosition().y);
  _mouse_context.right_down_pick = _pick(position);

  const auto handle_dialog(
      [this](smeeze::wx::node_editor::dialogs::base &&dlg) {
        _process("show dialog", [&]() {
          if (dlg.is_valid()) {
            dlg.ShowModal();
            if (dlg.has_modified_model()) {
              recompute();
              Refresh();
            }
          }
        });
      });
  const auto handle_menu([&](smeeze::wx::node_editor::menus::base &&menu) {
    _process("show menu", [&]() {
      if (menu.is_valid()) {
        PopupMenu(&menu, wxPoint(position.x(), position.y()));
        if (menu.has_modified_model()) {
          recompute();
          Refresh();
        }
      }
    });
  });

  switch (_mouse_context.right_down_pick.mode) {
  case smeeze::wx::node_editor::view::pick::mode_t::none:
    break;
  case smeeze::wx::node_editor::view::pick::mode_t::node:
    handle_menu(smeeze::wx::node_editor::menus::node(
        this, _model, _mouse_context.right_down_pick.node_id));
    break;
  case smeeze::wx::node_editor::view::pick::mode_t::input:
    break;
  case smeeze::wx::node_editor::view::pick::mode_t::output:
    handle_dialog(smeeze::wx::node_editor::dialogs::output(
        _model, _mouse_context.right_down_pick.node_id,
        _mouse_context.right_down_pick.output_id,
        smeeze::geom::vec2<float>(GetScreenPosition().x,
                                  GetScreenPosition().y) +
            position,
        smeeze::geom::vec2<float>(800, 600)));
    break;
  case smeeze::wx::node_editor::view::pick::mode_t::connection:
    handle_menu(smeeze::wx::node_editor::menus::connection(
        _model, _mouse_context.right_down_pick.connection));
    break;
  }
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::view::_mouse_motion(wxMouseEvent &event) {
  const smeeze::geom::vec2<float> position(event.GetPosition().x,
                                           event.GetPosition().y);
  const auto position_delta(position - _mouse_context.motion_pick.position);
  const auto current_pick(_pick(position));

  _mouse_context.motion_pick = current_pick;
  _mouse_context.connection_is_valid = false;

  switch (_mouse_context.left_down_pick.mode) {
  case smeeze::wx::node_editor::view::pick::mode_t::none:
    if (event.LeftIsDown()) {
      wxMoveEvent e(wxPoint(position_delta.x(), position_delta.y()));
      ProcessWindowEvent(e);
      _mouse_context.motion_pick.position -= position_delta;
    } else {
      const auto p(_pick(position));
      switch (p.mode) {
      case pick::mode_t::none:
        UnsetToolTip();
        break;
      case pick::mode_t::node:
        _process("set node tooltip",
                 [&]() { SetToolTip(_model->node_info(p.node_id)); });
        break;
      case pick::mode_t::input:
        _process("set input tooltip", [&]() {
          SetToolTip(_model->input_info(p.node_id, p.input_id));
        });
        break;
      case pick::mode_t::output:
        _process("set output tooltip", [&]() {
          SetToolTip(_model->output_info(p.node_id, p.output_id));
        });
        break;
      case pick::mode_t::connection:
        break;
      }
    }
    break;
  case smeeze::wx::node_editor::view::pick::mode_t::node:
    _process("set node position", [&]() {
      _model->node_inc_position(_mouse_context.left_down_pick.node_id,
                                position_delta);
      recompute();
      ;
    });
    break;
  case smeeze::wx::node_editor::view::pick::mode_t::input:
    _mouse_context.connection_is_valid =
        _is_valid_connection(current_pick, _mouse_context.left_down_pick);
    break;
  case smeeze::wx::node_editor::view::pick::mode_t::output:
    _mouse_context.connection_is_valid =
        _is_valid_connection(_mouse_context.left_down_pick, current_pick);
    break;
  default:
    break;
  }

  Refresh();
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::view::_process(
    const std::string &msg, const std::function<void()> &f) const {
  try {
    _status_bar->SetStatusText(msg);
    f();
    _status_bar->SetStatusText("...");
  } catch (const std::exception &e) {
    std::cerr << msg << ": error: " << e.what() << std::endl;
    _status_bar->SetStatusText(e.what());
  }
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::view::pick smeeze::wx::node_editor::view::_pick(
    const smeeze::geom::vec2<float> &position) const {
  size_t idx;
  pick rv;
  rv.position = position;

  // nodes
  const auto &nodes(_model->get_nodes());
  idx = nodes.size();
  while (idx > 0) {
    --idx;
    const auto &node(nodes.at(idx));
    if (node.get_rect().intersects(position)) {
      rv.node_id = node.get_id();

      for (const auto &c : node.get_inputs()) {
        if (c.rect.intersects(position)) {
          rv.connector_rect = c.rect;
          rv.input_id = c.id;
          rv.mode = pick::mode_t::input;
          return rv;
        }
      }

      for (const auto &c : node.get_outputs()) {
        if (c.rect.intersects(position)) {
          rv.connector_rect = c.rect;
          rv.output_id = c.id;
          rv.mode = pick::mode_t::output;
          return rv;
        }
      }

      rv.mode = pick::mode_t::node;
      return rv;
    }
  }

  // connections
  float prox(1e6);
  for (const auto &c : _precomputed_connections) {
    for (const auto &l : c.lines) {
      float f;
      if (l.proximity(position, 16, f) && f < prox) {
        prox = f;
        rv.connection = c.con;
        rv.mode = pick::mode_t::connection;
      }
    }
  }

  return rv;
}
///////////////////////////////////////////////////////////////////////////////
bool smeeze::wx::node_editor::view::_is_valid_connection(
    const smeeze::wx::node_editor::view::pick &p0,
    const smeeze::wx::node_editor::view::pick &p1) const {
  return (p0.mode == smeeze::wx::node_editor::view::pick::mode_t::output &&
          p1.mode == smeeze::wx::node_editor::view::pick::mode_t::input) &&
         _model->connection_is_valid(
             {p0.node_id, p0.output_id, p1.node_id, p1.input_id});
  ;
}
///////////////////////////////////////////////////////////////////////////////
bool smeeze::wx::node_editor::view::_is_selected(
    const smeeze::wx::node_editor::node &nw,
    const smeeze::wx::node_editor::view::pick &p) {
  switch (p.mode) {
  case smeeze::wx::node_editor::view::pick::mode_t::node:
  case smeeze::wx::node_editor::view::pick::mode_t::input:
  case smeeze::wx::node_editor::view::pick::mode_t::output:
    if (nw.get_id() == p.node_id) {
      return true;
    }
    break;
  default:
    break;
  }
  return false;
}
///////////////////////////////////////////////////////////////////////////////
bool smeeze::wx::node_editor::view::_is_selected(
    const smeeze::wx::node_editor::connection &c,
    const smeeze::wx::node_editor::view::pick &p) {
  switch (p.mode) {
  case smeeze::wx::node_editor::view::pick::mode_t::connection:
    if (c == p.connection) {
      return true;
    }
    break;
  default:
    break;
  }
  return false;
}
