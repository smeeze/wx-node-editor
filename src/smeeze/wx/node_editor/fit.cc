// smeeze wx
#include <smeeze/wx/node_editor/fit.h>
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::geom::point::point(float x, float y) : data{x, y} {}
///////////////////////////////////////////////////////////////////////////////
const float &smeeze::wx::node_editor::geom::point::operator[](size_t i) const {
  return data[i];
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::geom::point &
smeeze::wx::node_editor::geom::point::operator-=(const point &rhs) {
  data[0] -= rhs[0];
  data[1] -= rhs[1];
  return *this;
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::geom::point
smeeze::wx::node_editor::geom::point::operator-(const point &rhs) const {
  return point(*this) -= rhs;
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::geom::point &
smeeze::wx::node_editor::geom::point::operator+=(const point &rhs) {
  data[0] += rhs[0];
  data[1] += rhs[1];
  return *this;
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::geom::point
smeeze::wx::node_editor::geom::point::operator+(const point &rhs) const {
  return point(*this) += rhs;
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::geom::point &
smeeze::wx::node_editor::geom::point::operator*=(const float &f) {
  data[0] *= f;
  data[1] *= f;
  return *this;
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::geom::point
smeeze::wx::node_editor::geom::point::operator*(const float &f) const {
  return point(*this) *= f;
}
///////////////////////////////////////////////////////////////////////////////
float smeeze::wx::node_editor::geom::point::length() const {
  return std::sqrt(data[0] * data[0] + data[1] * data[1]);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::fit::add_connection(const connection &c) {
  if (!_has_connection(c)) {
    _connections.push_back(c);
  }
}
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::fit::solution
smeeze::wx::node_editor::fit::solve() const {
  auto solutions(_solve());

  std::sort(std::begin(solutions), std::end(solutions),
            [](const solution &l, const solution &r) {
              return l.crossings < r.crossings;
            });
  return solutions.front();
}
///////////////////////////////////////////////////////////////////////////////
std::vector<smeeze::wx::node_editor::fit::solution>
smeeze::wx::node_editor::fit::_solve() const {
  std::vector<solution> solutions;

  _solve_recursive({}, solutions, 0, _get_max_rank());

  return solutions;
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::fit::_solve_recursive(
    const solution &sol, std::vector<solution> &solutions,
    uint32_t current_rank, uint32_t max_rank) const {
  std::vector<std::string> nodes;
  for (const auto &node : _get_nodes()) {
    if (_get_rank(node) == current_rank) {
      nodes.push_back(node);
    }
  }
  // for each permutation...
  do {
    solution sol_copy(sol);
    auto nodes_sorted(nodes);
    std::sort(nodes_sorted.begin(), nodes_sorted.end());
    // place nodes
    for (const auto &node : nodes_sorted) {
      uint32_t order(0);
      for (const auto &fix : sol_copy.fixed_nodes) {
        if (fix.rank == current_rank) {
          ++order;
        }
      }
      sol_copy.fixed_nodes.push_back(placement{current_rank, order, node});
    }
    // recurse
    if (current_rank < max_rank) {
      _solve_recursive(sol_copy, solutions, current_rank + 1, max_rank);
    } else {
      _count_crossings(sol_copy);
      // store
      solutions.push_back(sol_copy);
    }
  } while (std::next_permutation(std::begin(nodes), std::end(nodes)));
}
///////////////////////////////////////////////////////////////////////////////
uint32_t
smeeze::wx::node_editor::fit::_get_rank(const std::string &id) const {
  uint32_t rv(0);
  for (const auto &l : _get_leaders(id)) {
    const auto r(_get_rank(l) + 1);
    if (r > rv) {
      rv = r;
    }
  }
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
uint32_t smeeze::wx::node_editor::fit::_get_max_rank() const {
  uint32_t rv(0);
  for (const auto &n : _get_nodes()) {
    const auto r(_get_rank(n));
    if (r > rv) {
      rv = r;
    }
  }
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
std::vector<std::string>
smeeze::wx::node_editor::fit::_get_leaders(const std::string &id) const {
  std::vector<std::string> rv;

  for (const auto &c : _connections) {
    if (c.right == id) {
      rv.push_back(c.left);
    }
  }

  std::sort(rv.begin(), rv.end());
  rv.erase(std::unique(rv.begin(), rv.end()), rv.end());

  return rv;
}
///////////////////////////////////////////////////////////////////////////////
std::vector<std::string> smeeze::wx::node_editor::fit::_get_nodes() const {
  std::vector<std::string> rv;

  for (const auto &c : _connections) {
    rv.push_back(c.left);
    rv.push_back(c.right);
  }

  std::sort(rv.begin(), rv.end());
  rv.erase(std::unique(rv.begin(), rv.end()), rv.end());

  return rv;
}
///////////////////////////////////////////////////////////////////////////////
bool smeeze::wx::node_editor::fit::_has_connection(
    const connection &c) const {
  for (const auto &i : _connections) {
    if (i == c) {
      return true;
    }
  }
  return false;
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::fit::_count_crossings(solution &sol) const {
  const auto get_rank_order([&](const std::string &id) -> geom::point {
    uint32_t rank(0);
    uint32_t order(0);
    for (const auto &fix : sol.fixed_nodes) {
      if (fix.node == id) {
        if (fix.order > order) {
          rank = fix.rank;
          order = fix.order;
        }
      }
    }
    return geom::point(rank, order);
  });
  const auto make_line([&](const geom::point &l, const geom::point &r) {
    std::vector<geom::point> rv;
    for (float n = 0; n <= 1.0; n += 0.1) {
      rv.push_back(l + (r - l) * n);
    }
    return rv;
  });
  for (const auto &con : _connections) {
    const auto points(
        make_line(get_rank_order(con.left), get_rank_order(con.right)));
    for (const auto &fix : sol.fixed_nodes) {
      geom::point center(fix.rank, fix.order);
      for (const auto &point : points) {
        if ((point - center).length() < 0.1) {
          ++sol.crossings;
        }
      }
    }
  }
}
