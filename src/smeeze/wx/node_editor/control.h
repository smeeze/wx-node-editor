#pragma once

// system
#include <functional>
// wx
#include <wx/listctrl.h>
#include <wx/statusbr.h>
#include <wx/wx.h>
// smeeze wx
#include <smeeze/wx/node_editor/model.h>
#include <smeeze/wx/node_editor/view.h>
#include <smeeze/wx/visibility.h>
///////////////////////////////////////////////////////////////////////////////
namespace smeeze {
namespace wx {
namespace node_editor {
class API_SMEEZE_WX control : public wxFrame {
public:
  control(smeeze::wx::node_editor::model *, wxWindow *parent, wxWindowID id,
          const wxString &title);

  void handle_drop(int, int, const std::string &);

protected:
  void _set_list_selection(const std::string &);
  void _process(const std::string &, const std::function<void()> &);

protected:
  smeeze::wx::node_editor::view *_view;
  smeeze::wx::node_editor::model *_model;
  wxListCtrl *_item_list_ctrl;
  wxStatusBar *_status_bar;
};
} // namespace node_editor
} // namespace wx
} // namespace smeeze
