// system
#include <fstream>
#include <stdexcept>
// self
#include <smeeze/wx/node_editor/model.h>
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::model::~model() {}
///////////////////////////////////////////////////////////////////////////////
std::vector<std::string>
smeeze::wx::node_editor::model::node_list_available() const {
  return _node_list_available();
}
///////////////////////////////////////////////////////////////////////////////
std::string smeeze::wx::node_editor::model::node_add(const std::string &s) {
  return _node_add(s);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::model::node_del(const std::string &id) {
  _node_del(id);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::model::node_rename(const std::string &from,
                                                 const std::string &to) {
  _node_rename(from, to);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::model::node_inc_position(
    const std::string &id, const smeeze::geom::vec2<float> &p) {
  _node_inc_position(id, p);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::model::node_set_position(
    const std::string &id, const smeeze::geom::vec2<float> &p) {
  _node_set_position(id, p);
}
///////////////////////////////////////////////////////////////////////////////
const std::vector<smeeze::wx::node_editor::node> &
smeeze::wx::node_editor::model::get_nodes() const {
  return _get_nodes();
}
///////////////////////////////////////////////////////////////////////////////
const smeeze::wx::node_editor::node &
smeeze::wx::node_editor::model::node_find(const std::string &id) {
  for (auto &n : _get_nodes()) {
    if (n.get_id() == id) {
      return n;
    }
  }
  throw(std::runtime_error("could not get node by id"));
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::model::node_to_back(const std::string &id) {
  _node_to_back(id);
}
///////////////////////////////////////////////////////////////////////////////
std::string smeeze::wx::node_editor::model::node_info(const std::string &id) {
  return _node_info(id);
}
///////////////////////////////////////////////////////////////////////////////
bool smeeze::wx::node_editor::model::connection_is_valid(
    const smeeze::wx::node_editor::connection &c) const {
  return _connection_is_valid(c);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::model::connection_add(
    const smeeze::wx::node_editor::connection &c) {
  _connection_add(c);
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::model::connection_del(
    const smeeze::wx::node_editor::connection &c) {
  _connection_del(c);
}
///////////////////////////////////////////////////////////////////////////////
const std::vector<smeeze::wx::node_editor::connection> &
smeeze::wx::node_editor::model::get_connections() const {
  return _get_connections();
}
///////////////////////////////////////////////////////////////////////////////
std::string
smeeze::wx::node_editor::model::input_info(const std::string &node_id,
                                           const std::string &input_id) {
  return _input_info(node_id, input_id);
}
///////////////////////////////////////////////////////////////////////////////
wxWindow *smeeze::wx::node_editor::model::get_output_window(
    wxWindow *parent, const std::string &node_id,
    const std::string &output_id) {
  return _get_output_window(parent, node_id, output_id);
}
///////////////////////////////////////////////////////////////////////////////
std::string
smeeze::wx::node_editor::model::output_info(const std::string &node_id,
                                            const std::string &output_id) {
  return _output_info(node_id, output_id);
}
///////////////////////////////////////////////////////////////////////////////
std::vector<std::string>
smeeze::wx::node_editor::model::_node_list_available() const {
  return {};
}
///////////////////////////////////////////////////////////////////////////////
std::string smeeze::wx::node_editor::model::_node_info(const std::string &) {
  return {};
}
///////////////////////////////////////////////////////////////////////////////
std::string smeeze::wx::node_editor::model::_input_info(const std::string &,
                                                        const std::string &) {
  return {};
}
///////////////////////////////////////////////////////////////////////////////
std::string smeeze::wx::node_editor::model::_output_info(const std::string &,
                                                         const std::string &) {
  return {};
}
///////////////////////////////////////////////////////////////////////////////
std::string smeeze::wx::node_editor::model::_get_filename_extension() const {
  return "smeeze wx(*.smwx)|*.smwx|any(*)|*";
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::model::_load(const std::string &f) {
  _clear();
  std::ifstream in(f);
  while (in) {
    std::string s;
    in >> s;
    if (s == "node") {
      std::string id;
      std::string name;
      float x;
      float y;
      in >> id >> name >> x >> y;
      // std::cout << "node: " << id << " " << name << " " << x << " " << y <<
      // std::endl;
      const auto id_tmp(_node_add(name));
      node_rename(id_tmp, id);
      node_set_position(id, {x, y});
    }
    if (s == "option") {
      std::string id;
      std::string option;
      std::string value;

      in >> id >> option >> value;
      // std::cout << "option: " << id << " " << option << " " << value <<
      // std::endl;
      set_option(id, option, value);
    }
    if (s == "connection") {
      smeeze::wx::node_editor::connection c;
      in >> c.node0 >> c.output >> c.node1 >> c.input;
      connection_add(c);
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::wx::node_editor::model::_save(const std::string &f) {
  std::ofstream out(f);

  for (const auto &n : get_nodes()) {
    out << "node " << n.get_id() << " " << n.get_name() << " "
        << n.get_position().x() << " " << n.get_position().y() << std::endl;
  }

  for (const auto &n : get_nodes()) {
    for (const auto &o : n.get_options()) {
      if (o.value.size()) {
        out << "option " << n.get_id() << " " << o.id << " " << o.value
            << std::endl;
      }
    }
  }

  for (const auto &n : get_connections()) {
    out << "connection " << n.node0 << " " << n.output << " " << n.node1 << " "
        << n.input << std::endl;
  }
}
