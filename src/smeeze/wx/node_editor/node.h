#pragma once

// system
#include <unordered_map>
// wx
#include <wx/gdicmn.h>
// smeeze wx
#include <smeeze/geom/line.h>
#include <smeeze/geom/rect.h>
#include <smeeze/wx/visibility.h>

namespace smeeze {
namespace wx {
namespace node_editor {
class API_SMEEZE_WX node {
public:
  enum class option_type { string, int_, float_, file_load, file_save };
  struct option {
    std::string id;
    std::string value;
    option_type type = option_type::string;
    std::unordered_map<std::string, std::string> params = {};
    std::string get_param(const std::string &key,
                          const std::string &default_value) const {
      auto i(params.find(key));
      if (i != std::end(params)) {
        return i->second;
      }
      return default_value;
    }
  };
  node(const std::string &id, const std::string &name,
       const std::vector<std::string> &inputs,
       const std::vector<std::string> &outputs,
       const std::vector<option> &options = {},
       const std::vector<std::string> &functions = {})
      : _id(id), _name(name), _inputs(inputs), _outputs(outputs),
        _options(options), _functions(functions) {}

  int get_radius() const;

  smeeze::geom::vec2<float> get_position() const;
  void inc_position(const smeeze::geom::vec2<float> &);
  void set_position(const smeeze::geom::vec2<float> &);

  const std::string &get_id() const;
  void set_id(const std::string &);
  const std::string &get_name() const;

  smeeze::geom::rect get_rect() const;
  bool intersects(const smeeze::geom::line &, float &,
                  smeeze::geom::vec2<float> &, smeeze::geom::line &) const;

  struct connector {
    std::string id;
    smeeze::geom::rect rect;
  };

  std::vector<connector> get_inputs() const;
  connector get_input_by_id(const std::string &) const;

  std::vector<connector> get_outputs() const;
  connector get_output_by_id(const std::string &) const;

  const std::vector<option> &get_options() const;
  option &get_option_by_id(const std::string &);

  const std::vector<std::string> &get_functions() const { return _functions; }

  smeeze::geom::vec2<float> get_width_offset() const;
  smeeze::geom::vec2<float> get_body_offset() const;

private:
  float _get_width() const;
  float _get_height() const;

private:
  std::string _id;
  std::string _name;
  std::vector<std::string> _inputs;
  std::vector<std::string> _outputs;
  std::vector<option> _options;
  std::vector<std::string> _functions;

  smeeze::geom::vec2<float> _position;

  int _space_x_count = 10;
  float _space_x = 16;
  float _space_y = 12;
  int _padding = 2;
  int _radius = _space_y - _padding;
};
} // namespace node_editor
} // namespace wx
} // namespace smeeze
