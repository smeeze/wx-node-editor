#pragma once

// system
#include <string>
#include <vector>
// wx
#include <wx/window.h>
// smeeze wx
#include <smeeze/geom/vec2.h>
#include <smeeze/wx/node_editor/connection.h>
#include <smeeze/wx/node_editor/node.h>
#include <smeeze/wx/visibility.h>

namespace smeeze {
namespace wx {
namespace node_editor {
class API_SMEEZE_WX model {
public:
  virtual ~model();

  //
  std::vector<std::string> node_list_available() const;

  //
  std::string node_add(const std::string &);
  void node_del(const std::string &);
  void node_rename(const std::string &, const std::string &);
  void node_inc_position(const std::string &,
                         const smeeze::geom::vec2<float> &);
  void node_set_position(const std::string &,
                         const smeeze::geom::vec2<float> &);
  const std::vector<smeeze::wx::node_editor::node> &get_nodes() const;
  const smeeze::wx::node_editor::node &node_find(const std::string &);
  void node_to_back(const std::string &);
  std::string node_info(const std::string &);

  //
  bool connection_is_valid(const smeeze::wx::node_editor::connection &) const;
  void connection_add(const connection &);
  void connection_del(const connection &);
  const std::vector<smeeze::wx::node_editor::connection> &
  get_connections() const;

  //
  void set_option(const std::string &node, const std::string &option,
                  const std::string &value) {
    _set_option(node, option, value);
  }

  //
  void call_function(const std::string &node_id, const std::string &function) {
    _call_function(node_id, function);
  }

  //
  std::string input_info(const std::string &, const std::string &);
  //
  wxWindow *get_output_window(wxWindow *, const std::string &,
                              const std::string &);
  std::string output_info(const std::string &, const std::string &);

  //
  std::string get_filename_extension() const {
    return _get_filename_extension();
  }
  void load(const std::string &filename) { _load(filename); }
  void save(const std::string &filename) { _save(filename); }
  void clear() { _clear(); }

protected:
  virtual std::vector<std::string> _node_list_available() const;
  virtual std::string _node_add(const std::string &) = 0;
  virtual void _node_del(const std::string &) = 0;
  virtual void _node_rename(const std::string &, const std::string &) = 0;
  virtual void _node_inc_position(const std::string &,
                                  const smeeze::geom::vec2<float> &) = 0;
  virtual void _node_set_position(const std::string &,
                                  const smeeze::geom::vec2<float> &) = 0;
  virtual const std::vector<smeeze::wx::node_editor::node> &
  _get_nodes() const = 0;
  virtual void _node_to_back(const std::string &) = 0;
  virtual std::string _node_info(const std::string &);

  virtual bool _connection_is_valid(const connection &) const = 0;
  virtual void _connection_add(const connection &) = 0;
  virtual void _connection_del(const connection &) = 0;
  virtual const std::vector<smeeze::wx::node_editor::connection> &
  _get_connections() const = 0;

  virtual void _set_option(const std::string &, const std::string &,
                           const std::string &) = 0;

  virtual void _call_function(const std::string &, const std::string &) {}

  virtual std::string _input_info(const std::string &, const std::string &);

  virtual wxWindow *_get_output_window(wxWindow *, const std::string &,
                                       const std::string &) {
    return nullptr;
  }
  virtual std::string _output_info(const std::string &, const std::string &);

  // virtual std::string _get_extension() const { return "any|.*;smwx|.smwx"; }
  virtual std::string _get_filename_extension() const;
  virtual void _load(const std::string &);
  virtual void _save(const std::string &);
  virtual void _clear() = 0;
};
} // namespace node_editor
} // namespace wx
} // namespace smeeze
