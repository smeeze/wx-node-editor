#pragma once

// smeeze wx
#include <smeeze/geom/vec2.h>
#include <smeeze/wx/node_editor/dialogs/base.h>
#include <smeeze/wx/node_editor/model.h>

namespace smeeze {
namespace wx {
namespace node_editor {
namespace dialogs {
class output : public smeeze::wx::node_editor::dialogs::base {
public:
  output(smeeze::wx::node_editor::model *, const std::string &,
         const std::string &, const smeeze::geom::vec2<float> &,
         const smeeze::geom::vec2<float> &);
};
} // namespace dialogs
} // namespace node_editor
} // namespace wx
} // namespace smeeze
