#pragma once

// wx
#include <wx/dialog.h>
// smeeze wx
#include <smeeze/geom/vec2.h>

namespace smeeze {
namespace wx {
namespace node_editor {
namespace dialogs {
class base : public wxDialog {
public:
  base(const std::string &, const smeeze::geom::vec2<float> &,
       const smeeze::geom::vec2<float> &);

  bool has_modified_model() const { return _has_modified_model; }
  bool is_valid() const { return _is_valid; }

protected:
  bool _has_modified_model = false;
  bool _is_valid = true;
};
} // namespace dialogs
} // namespace node_editor
} // namespace wx
} // namespace smeeze
