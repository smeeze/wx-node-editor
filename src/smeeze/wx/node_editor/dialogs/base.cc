// smeeze wx
#include <smeeze/wx/node_editor/dialogs/base.h>
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::dialogs::base::base(
    const std::string &title, const smeeze::geom::vec2<float> &pos,
    const smeeze::geom::vec2<float> &size)
    : wxDialog(nullptr,                                 //
               wxID_ANY,                                //
               title,                                   //
               wxPoint(pos.x(), pos.y()),               //
               wxSize(size.x(), size.y()),              //
               wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER //
      ) {}
