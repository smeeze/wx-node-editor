// wx
#include <wx/button.h>
#include <wx/sizer.h>
// smeeze wx
#include <smeeze/wx/node_editor/dialogs/output.h>
///////////////////////////////////////////////////////////////////////////////
smeeze::wx::node_editor::dialogs::output::output(
    smeeze::wx::node_editor::model *model, const std::string &node_id,
    const std::string &output_id, const smeeze::geom::vec2<float> &pos,
    const smeeze::geom::vec2<float> &size)
    : smeeze::wx::node_editor::dialogs::base("output", pos, size) {
  auto w(model->get_output_window(this, node_id, output_id));
  if (w == nullptr) {
    _is_valid = false;
  } else {
    wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
    vbox->Add(w, 10, wxEXPAND);

    auto button_close = new wxButton(this, wxID_ANY, "close");
    vbox->Add(button_close, 1, wxTILE);
    button_close->Bind(wxEVT_BUTTON,
                       [this](wxCommandEvent &) { this->Close(); });

    this->SetSizer(vbox);

    Fit();
    button_close->SetFocus();
  }
}
