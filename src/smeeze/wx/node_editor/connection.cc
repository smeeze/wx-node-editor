// smeeze wx
#include <smeeze/wx/node_editor/connection.h>
///////////////////////////////////////////////////////////////////////////////
bool smeeze::wx::node_editor::connection::operator==(
    const smeeze::wx::node_editor::connection &rhs) const {
  return node0 == rhs.node0 && output == rhs.output && node1 == rhs.node1 &&
         input == rhs.input;
}
