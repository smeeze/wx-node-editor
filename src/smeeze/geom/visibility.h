#pragma once

#ifdef _WIN32
#define API_SMEEZE_GEOM_EXPORT_BASE __declspec(dllexport)
#define API_SMEEZE_GEOM_IMPORT_BASE
#else
#define API_SMEEZE_GEOM_EXPORT_BASE __attribute__((visibility("default")))
#define API_SMEEZE_GEOM_IMPORT_BASE
#endif

#ifdef SMEEZE_GEOM_EXPORT
#define API_SMEEZE_GEOM API_SMEEZE_GEOM_EXPORT_BASE
#else
#define API_SMEEZE_GEOM API_SMEEZE_GEOM_IMPORT_BASE
#endif
