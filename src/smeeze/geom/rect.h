#pragma once

// smeeze geom
#include <smeeze/geom/vec2.h>
#include <smeeze/geom/visibility.h>

namespace smeeze {
namespace geom {
class API_SMEEZE_GEOM rect {
public:
  // TODO: const?
  smeeze::geom::vec2<float> p0;
  smeeze::geom::vec2<float> p1;

  const smeeze::geom::vec2<float> &get_top_left() const;
  smeeze::geom::vec2<float> get_top_right() const;
  const smeeze::geom::vec2<float> &get_bottom_right() const;
  smeeze::geom::vec2<float> get_bottom_left() const;

  smeeze::geom::vec2<float> get_center() const;

  float x() const;
  float y() const;
  float w() const;
  float h() const;

  bool intersects(const smeeze::geom::vec2<float> &) const;
};
} // namespace geom
} // namespace smeeze
