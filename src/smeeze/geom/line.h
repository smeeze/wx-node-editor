#pragma once

// smeeze geom
#include <smeeze/geom/vec2.h>
#include <smeeze/geom/visibility.h>

namespace smeeze {
namespace geom {
class API_SMEEZE_GEOM line {
public:
  smeeze::geom::vec2<float> p0;
  smeeze::geom::vec2<float> p1;

  smeeze::geom::vec2<float> direction() const;
  smeeze::geom::vec2<float> normal() const;

  bool proximity(const smeeze::geom::vec2<float> &, float, float &) const;
  bool intersects(const line &, float &, smeeze::geom::vec2<float> &) const;

  float length() const;

  line resize(float) const;
  line rotate(float) const;
};
} // namespace geom
} // namespace smeeze
