// smeeze geom
#include <smeeze/geom/line.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
smeeze::geom::vec2<float> _closest_point(const smeeze::geom::vec2<float> &p0,
                                         const smeeze::geom::vec2<float> &p1,
                                         const smeeze::geom::vec2<float> &p) {
  if (p0.x() == p1.x()) {
    return {p0.x(), p.y()};
  }
  if (p0.y() == p1.y()) {
    return {p.x(), p1.y()};
  }
  const float m1((p1.y() - p0.y()) / (p1.x() - p0.x()));
  const float m2(-1.0 / m1);
  const float x((m1 * p0.x() - m2 * p.x() + p.y() - p0.y()) / (m1 - m2));
  const float y(m2 * (x - p.x()) + p.y());
  return {x, y};
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
smeeze::geom::vec2<float> smeeze::geom::line::direction() const {
  return p1 - p0;
}
///////////////////////////////////////////////////////////////////////////////
smeeze::geom::vec2<float> smeeze::geom::line::normal() const {
  const auto d(direction());
  const auto l(length());
  return d * (1.0 / l);
  // return {d.x / l, d.y / l};
}
///////////////////////////////////////////////////////////////////////////////
bool smeeze::geom::line::proximity(const smeeze::geom::vec2<float> &p,
                                   float threshold, float &result) const {
  const auto cp(_closest_point(p0, p1, p));
  const auto f(smeeze::geom::line{p0, cp}.length());
  if (f < 0.0 || f > length()) {
    return false;
  }
  const smeeze::geom::line l{cp, p};
  result = l.length();
  return (l.length() < threshold) && ((p1 - p0).dot(p - p0) >= 0);
}
///////////////////////////////////////////////////////////////////////////////
bool smeeze::geom::line::intersects(
    const line &l, float &f, smeeze::geom::vec2<float> &intersection) const {
  const auto b(direction());
  const auto d(l.direction());
  float dot_product(b.x() * d.y() - b.y() * d.x());

  if (dot_product == 0) {
    return false;
  }

  const smeeze::geom::vec2<float> c(l.p0 - p0);
  const float t((c.x() * d.y() - c.y() * d.x()) / dot_product);
  if (t < 0 || t > 1) {
    return false;
  }

  const float u((c.x() * b.y() - c.y() * b.x()) / dot_product);
  if (u < 0 || u > 1) {
    return false;
  }

  f = t;
  intersection = p0 + b * t;

  return true;
}
///////////////////////////////////////////////////////////////////////////////
float smeeze::geom::line::length() const { return (p1 - p0).length(); }
///////////////////////////////////////////////////////////////////////////////
smeeze::geom::line smeeze::geom::line::resize(float f) const {
  return {p0, p0 + normal() * f};
}
///////////////////////////////////////////////////////////////////////////////
smeeze::geom::line smeeze::geom::line::rotate(float r) const {
  const auto n(normal());
  const auto angle(std::atan2(n.y(), n.x()) + r);
  return {p0, p0 + smeeze::geom::vec2<float>(std::cos(angle), std::sin(angle)) *
                       length()};
}
