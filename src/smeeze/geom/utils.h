#pragma once

// system
#include <vector>
// smeeze geom
#include <smeeze/geom/line.h>
#include <smeeze/geom/rect.h>
#include <smeeze/geom/visibility.h>

namespace smeeze {
namespace geom {
class API_SMEEZE_GEOM utils {
public:
  static std::vector<smeeze::geom::line>
  to_lines(const std::vector<smeeze::geom::vec2<float>> &);

  static std::vector<smeeze::geom::vec2<float>>
  interpolate(const std::vector<smeeze::geom::vec2<float>> &, int);
  static std::vector<smeeze::geom::line>
  interpolate(const smeeze::geom::vec2<float> &,
              const smeeze::geom::vec2<float> &, int);
  static std::vector<smeeze::geom::line>
  interpolate(const smeeze::geom::rect &, const smeeze::geom::rect &, int);
};
} // namespace geom
} // namespace smeeze
