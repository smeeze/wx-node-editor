// smeeze geom
#include <smeeze/geom/utils.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
smeeze::geom::vec2<float> catmull_rom(float t,
                                      const smeeze::geom::vec2<float> &P0,
                                      const smeeze::geom::vec2<float> &P1,
                                      const smeeze::geom::vec2<float> &P2,
                                      const smeeze::geom::vec2<float> &P3) {
  float t2 = t * t;
  float t3 = t2 * t;
  return ((P1 * 2.0f) + (P2 - P0) * t +
          (P0 * 2.0f - P1 * 5.0f + P2 * 4.0f - P3) * t2 +
          (P1 * 3.0f - P0 - P2 * 3.0f + P3) * t3) *
         0.5f;
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
std::vector<smeeze::geom::line> smeeze::geom::utils::to_lines(
    const std::vector<smeeze::geom::vec2<float>> &points) {
  std::vector<smeeze::geom::line> rv;
  for (size_t n = 1; n < points.size(); ++n) {
    rv.emplace_back(smeeze::geom::line{points[n - 1], points[n]});
  }
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
std::vector<smeeze::geom::vec2<float>> smeeze::geom::utils::interpolate(
    const std::vector<smeeze::geom::vec2<float>> &pv, int count) {
  std::vector<smeeze::geom::vec2<float>> rv;

  for (int n = 0; n < count; ++n) {
    const float f((float)pv.size() * (float)n / (float)count);
    const float fl(floor(f));
    const float t(f - fl);
    const int idx((int)fl);

    rv.emplace_back(catmull_rom(t, pv[std::max(0, idx - 1)], pv[idx],
                                pv[std::min<int>(idx + 1, pv.size() - 1)],
                                pv[std::min<int>(idx + 2, pv.size() - 1)]));
  }

  return rv;
}
///////////////////////////////////////////////////////////////////////////////
std::vector<smeeze::geom::line>
smeeze::geom::utils::interpolate(const smeeze::geom::vec2<float> &a,
                                 const smeeze::geom::vec2<float> &b,
                                 int count) {
  std::vector<smeeze::geom::vec2<float>> points;
  const auto d(b - a);

  if (b.x() > a.x()) {
    const int x_displ(32);
    points.emplace_back(a);
    points.emplace_back(
        smeeze::geom::vec2<float>{a.x() + x_displ, a.y() + d.y() / 10});
    points.emplace_back(
        smeeze::geom::vec2<float>{b.x() - x_displ, b.y() - d.y() / 10});
    points.emplace_back(b);
  } else {
    const int x_displ(32);
    points.emplace_back(a);
    points.emplace_back(
        smeeze::geom::vec2<float>{a.x() + x_displ, a.y() + d.y() / 10});
    points.emplace_back(
        smeeze::geom::vec2<float>{a.x() + x_displ / 2, a.y() + d.y() / 3});
    points.emplace_back(
        smeeze::geom::vec2<float>{b.x() - x_displ / 2, b.y() - d.y() / 3});
    points.emplace_back(
        smeeze::geom::vec2<float>{b.x() - x_displ, b.y() - d.y() / 10});
    points.emplace_back(b);
  }

  return to_lines(smeeze::geom::utils::interpolate(points, count));
}
///////////////////////////////////////////////////////////////////////////////
std::vector<smeeze::geom::line>
smeeze::geom::utils::interpolate(const smeeze::geom::rect &a,
                                 const smeeze::geom::rect &b, int count) {
  return interpolate(a.get_center(), b.get_center(), count);
}
