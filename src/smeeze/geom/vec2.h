#pragma once

// system
#include <cassert>
#include <cmath>

#include <array>
#include <limits>

namespace smeeze {
namespace geom {
template <typename T = float> class vec2 {
public:
  vec2(const T &x = 0, const T &y = 0) : _data({{x, y}}) {}
  /*
  template <typename... Args>
  vec2(Args &&...args) : _data(std::forward<Args>(args)...) {}
  */

  T &operator[](int index) {
    assert(index < 2);
    return _data[index];
  }
  const T &operator[](int index) const {
    assert(index < 2);
    return _data[index];
  }

  const T &x() const { return _data[0]; }
  const T &y() const { return _data[1]; }

  bool operator==(const vec2 &v) const {
    return _compare(_data[0], v[0]) && _compare(_data[1], v[1]);
  }
  bool operator!=(const vec2 &v) const {
    return !_compare(_data[0], v[0]) || !_compare(_data[1], v[1]);
  }

  // math scalar
  vec2 operator-(const T &t) const { return vec2(*this) -= t; }
  vec2 &operator-=(const T &t) {
    _data[0] -= t;
    _data[1] -= t;
    return *this;
  }
  vec2 operator+(const T &t) const { return vec2(*this) += t; }
  vec2 &operator+=(const T &t) {
    _data[0] += t;
    _data[1] += t;
    return *this;
  }
  vec2 operator*(const T &t) const { return vec2(*this) *= t; }
  vec2 &operator*=(const T &t) {
    _data[0] *= t;
    _data[1] *= t;
    return *this;
  }
  vec2 operator/(const T &t) const { return vec2(*this) /= t; }
  vec2 &operator/=(const T &t) {
    _data[0] /= t;
    _data[1] /= t;
    return *this;
  }
  // math vec2
  vec2 operator-(const vec2 &o) const { return vec2(*this) -= o; }
  vec2 &operator-=(const vec2 &o) {
    _data[0] -= o[0];
    _data[1] -= o[1];
    return *this;
  }
  vec2 operator+(const vec2 &o) const { return vec2(*this) += o; }
  vec2 &operator+=(const vec2 &o) {
    _data[0] += o[0];
    _data[1] += o[1];
    return *this;
  }
  T dot(const vec2 &v) const { return _data[0] * v[0] + _data[1] * v[1]; }
  T length() const { return sqrt(_data[0] * _data[0] + _data[1] * _data[1]); }
  vec2 normalize() const { return vec2(*this) / length(); }

  vec2 rotate(const T &t) const {
    const auto c(std::cos(t));
    const auto s(std::sin(t));
    return {_data[0] * c - _data[1] * s, _data[0] * s + _data[1] * c};
  }

  vec2 snap(int mod) const {
    return {x() - ((int)x() % mod), y() - ((int)y() % mod)};
  }

private:
  bool _compare(const T &x, const T &y) const {
    return (std::abs(x - y) <= std::numeric_limits<T>::epsilon());
  }

private:
  std::array<T, 2> _data;
};
} // namespace geom
} // namespace smeeze
