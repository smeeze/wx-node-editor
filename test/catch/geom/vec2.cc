// catch
#include <catch2/catch_all.hpp>
// erptools
#include <smeeze/geom/vec2.h>
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("smeeze-geom-vec2") {
  smeeze::geom::vec2<float> v2(2, 0);
  REQUIRE(v2.length() == 2.0);
  REQUIRE(v2.normalize().length() == 1.0);

  const auto v2_r(v2.rotate(M_PI / 2.0));
  REQUIRE(v2_r.length() == 2.0);
  REQUIRE(std::roundf(v2_r.x()) == 0.0);
  REQUIRE(std::roundf(v2_r.y()) == 2.0);
}
